
-- ADMIN ---------------------------------------------------------

CREATE TABLE `admin`
(
 `id`       int NOT NULL ,
 `name`     varchar(45) NOT NULL ,
 `email`    varchar(45) NOT NULL ,
 `password` varchar(45) NOT NULL ,

PRIMARY KEY (`id`),
KEY `PKadmin` (`id`)
);


-- CATEGORY ---------------------------------------------------------
CREATE TABLE `category`
(
 `categoryID` int NOT NULL ,
 `title`      varchar(45) NOT NULL ,

PRIMARY KEY (`categoryID`),
KEY `PKcat` (`categoryID`)
);

-- FOOD ---------------------------------------------------------
CREATE TABLE `Food`
(
 `foodID`         int NOT NULL ,
 `name`           varchar(45) NOT NULL ,
 `description`    varchar(45) NOT NULL ,
 `startavailable` datetime NOT NULL ,
 `endavailable`   datetime NOT NULL ,
 `quantity`       int NOT NULL ,
 `image`          varchar(500) NOT NULL ,
 `categoryID`     int NOT NULL ,
 `userID`         int NOT NULL ,
 `statusId`       int NOT NULL ,

PRIMARY KEY (`foodID`),
KEY `PKfood` (`foodID`)
);

-- RECIPIENT ---------------------------------------------------------
CREATE TABLE `Recipient`
(
 `recipientID` int NOT NULL ,
 `userID`      int NOT NULL ,
 `foodID`      int NOT NULL ,

PRIMARY KEY (`recipientID`),
KEY `PKrec` (`recipientID`)
);

-- REVIEWS ---------------------------------------------------------
CREATE TABLE `Reviews`
(
 `reviewID`    int NOT NULL ,
 `comment`     varchar(200) NOT NULL ,
 `rating`      int NOT NULL ,
 `date_rev`    datetime NOT NULL ,
 `recipientID` int NOT NULL ,
 `foodID`      int NOT NULL ,

PRIMARY KEY (`reviewID`),
KEY `PKreview` (`reviewID`)
);

-- STATUS ---------------------------------------------------------
CREATE TABLE `status`
(
 `statusId` int NOT NULL ,
 `name`     varchar(45) NOT NULL ,

PRIMARY KEY (`statusId`),
KEY `PKstatus` (`statusId`)

);
-- USERS ---------------------------------------------------------
CREATE TABLE `users`
(
 `userID`   int NOT NULL ,
 `name`     varchar(45) NOT NULL ,
 `email`    varchar(45) NOT NULL ,
 `phoneNo`  int NOT NULL ,
 `address`  varchar(45) NOT NULL ,
 `password` varchar(45) NOT NULL ,
 `image`    varchar(500) NOT NULL ,

PRIMARY KEY (`userID`),
KEY `PKuser` (`userID`)
);

-- WISHLIST ---------------------------------------------------------
CREATE TABLE `Wishlist`
(
 `wishid`     int NOT NULL ,
 `insertdate` date NOT NULL ,
 `userID`     int NOT NULL ,
 `foodID`     int NOT NULL ,
PRIMARY KEY (`wishid`),
KEY `PKwish` (`wishid`)
);




ALTER TABLE `admin`
 MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `Food`
 MODIFY `foodID` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `Recipient`
 MODIFY `recipientID` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `category`
 MODIFY `categoryID` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `users`
 MODIFY `userID` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `Reviews`
 MODIFY `reviewID` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `Wishlist`
 MODIFY `wishid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
ALTER TABLE `status`
 MODIFY `statusId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- FOOD ---------------------------------------------------------
ALTER TABLE `Food`
 ADD CONSTRAINT `PKcat` FOREIGN KEY (`categoryID`) REFERENCES `category` (`categoryID`),
 ADD CONSTRAINT `PKuser` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`),
 ADD CONSTRAINT `PKstatus` FOREIGN KEY (`statusId`) REFERENCES `status` (`statusId`);

-- WIXHLIST ---------------------------------------------------------
ALTER TABLE `Wishlist`
  ADD CONSTRAINT `PKuser1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`),
  ADD CONSTRAINT `PKfood` FOREIGN KEY (`foodID`) REFERENCES `Food` (`foodID`);


-- RECIPIENT ---------------------------------------------------------
ALTER TABLE `Recipient`
  ADD CONSTRAINT `PKfood1` FOREIGN KEY (`foodID`) REFERENCES `Food` (`foodID`);
  ADD CONSTRAINT `PKuser1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);
