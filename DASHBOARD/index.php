<?php
  include_once'Xconnection.php';
  session_start();
?>
	<!DOCTYPE html>
	<html lang="en">
	<!-- Title Page-->
	<title>Admin | Dashboard</title>

	<body class="animsition">
		<div class="page-wrapper">
			<!-- HEADER MOBILE-->
			<!-- END HEADER MOBILE-->
			<?php include_once'header.php';?>
				<!-- MENU SIDEBAR-->
				<!-- END MENU SIDEBAR-->
				<!-- PAGE CONTAINER-->
				<div class="page-container">
					<!-- PAGE CONTAINER-->
					<!-- HEADER DESKTOP-->
					<?php include_once'headerD.php'; ?>
						<!-- END HEADER DESKTOP-->
						<!-- MAIN CONTENT-->
						<div class="main-content">
							<div class="section__content section__content--p30">
								<div class="container-fluid">
									<div class="row m-t-25">
										<!-- START ROW -->
										<div class="col-xl-3">
											<div class="card" style="width:">
												<div class="card-header" style="background-color:#7855c4"> <strong class="card-title text-light">Total Users</strong> </div>
												<div class="card-body text-white" style="background-color:#7caacf">
													<div class="row no-gutters align-items-center">
														<div class="col mr-2">
															<h2>
                                                    <?php $donor = $connection->query("SELECT COUNT(userID) AS totaluser FROM users");
                                                    while ($fetch = $donor->fetch_array()) { ?>
                                                    <?php echo $fetch['totaluser'];?>
                                                    <?php } ?>
                                                </h2> </div>
														<div class="col-auto"> <i class="fas fa-user-shield fa-2x text-gray-300"></i> </div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xl-3">
											<div class="card" style="width:">
												<div class="card-header" style="background-color:#7855c4"> <strong class="card-title text-light">Sharer</strong> </div>
												<div class="card-body text-white" style="background-color:#7caacf">
													<div class="row no-gutters align-items-center">
														<div class="col mr-2">
															<h2>
															<?php $donor = $connection->query("SELECT COUNT(DISTINCT userID)  AS totalsharer FROM food");
                                                    while ($fetch = $donor->fetch_array()) { ?>
                                                    <?php echo $fetch['totalsharer'];?>
                                                    <?php } ?>
                                                </h2> </div>
														<div class="col-auto"> <i class="fas fa-gifts fa-2x text-gray-300"></i> </div>
													</div>
												</div>
											</div>
                                        </div>
                                        <div class="col-xl-3">
											<div class="card" style="width:">
												<div class="card-header" style="background-color:#7855c4"> <strong class="card-title text-light">Receiver</strong> </div>
												<div class="card-body text-white" style="background-color:#7caacf">
													<div class="row no-gutters align-items-center">
														<div class="col mr-2">
															<h2>
															<?php $donor = $connection->query("SELECT COUNT(recipientID) AS totalrecipient FROM recipient");
                                                    while ($fetch = $donor->fetch_array()) { ?>
                                                    <?php echo $fetch['totalrecipient'];?>
                                                    <?php } ?>
                                                </h2> </div>
														<div class="col-auto"> <i class="fas fa-gifts fa-2x text-gray-300"></i> </div>
													</div>
												</div>
											</div>
                                        </div>
                                        <div class="col-xl-3">
											<div class="card" style="width:">
												<div class="card-header" style="background-color:#7855c4"> <strong class="card-title text-light">Total Goods</strong> </div>
												<div class="card-body text-white" style="background-color:#7caacf">
													<div class="row no-gutters align-items-center">
														<div class="col mr-2">
															<h2>
															<?php $donor = $connection->query("SELECT COUNT(foodID) AS totalfood FROM food");
                                                    while ($fetch = $donor->fetch_array()) { ?>
                                                    <?php echo $fetch['totalfood'];?>
                                                    <?php } ?>
                                                </h2> </div>
														<div class="col-auto"> <i class="fas fa-gifts fa-2x text-gray-300"></i> </div>
													</div>
												</div>
											</div>
										</div>
                                    </div> <!-- END ROW  T25-->   
                                    
                                    
                                    <div class="row m-t-25">
										<!-- START ROW -->
										<div class="col-xl-3">
											<div class="card" style="width:">
												<div class="card-header" style="background-color:#b43e8f"> <strong class="card-title text-light">Dry Food</strong> </div>
												<div class="card-body text-white" style="background-color:#858ae3">
													<div class="row no-gutters align-items-center">
														<div class="col mr-2">
															<h2>
															<?php $donor = $connection->query("SELECT COUNT(categoryID) AS totalcat FROM food WHERE categoryID = 1");
                                                    while ($fetch = $donor->fetch_array()) { ?>
                                                    <?php echo $fetch['totalcat'];?>
                                                    <?php } ?>
                                                </h2> </div>
														<div class="col-auto"> <i class="fas fa-user-shield fa-2x text-gray-300"></i> </div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xl-3">
											<div class="card" style="width:">
												<div class="card-header" style="background-color:#b43e8f"> <strong class="card-title text-light">Wet Food</strong> </div>
												<div class="card-body text-white" style="background-color:#858ae3">
													<div class="row no-gutters align-items-center">
														<div class="col mr-2">
															<h2>
															<?php $donor = $connection->query("SELECT COUNT(categoryID) AS totalcat FROM food WHERE categoryID = 2");
                                                    while ($fetch = $donor->fetch_array()) { ?>
                                                    <?php echo $fetch['totalcat'];?>
                                                    <?php } ?>
                                                </h2> </div>
														<div class="col-auto"> <i class="fas fa-gifts fa-2x text-gray-300"></i> </div>
													</div>
												</div>
											</div>
                                        </div>
                                        <div class="col-xl-3">
											<div class="card" style="width:">
												<div class="card-header" style="background-color:#b43e8f"> <strong class="card-title text-light">Drink</strong> </div>
												<div class="card-body text-white" style="background-color:#858ae3">
													<div class="row no-gutters align-items-center">
														<div class="col mr-2">
															<h2>
															<?php $donor = $connection->query("SELECT COUNT(categoryID) AS totalcat FROM food WHERE categoryID = 3");
                                                    while ($fetch = $donor->fetch_array()) { ?>
                                                    <?php echo $fetch['totalcat'];?>
                                                    <?php } ?>
                                                </h2> </div>
														<div class="col-auto"> <i class="fas fa-gifts fa-2x text-gray-300"></i> </div>
													</div>
												</div>
											</div>
                                        </div>
                                        <div class="col-xl-3">
											<div class="card" style="width:">
												<div class="card-header" style="background-color:#b43e8f"> <strong class="card-title text-light">Comment</strong> </div>
												<div class="card-body text-white" style="background-color:#858ae3">
													<div class="row no-gutters align-items-center">
														<div class="col mr-2">
															<h2>
															<?php $donor = $connection->query("SELECT COUNT(userID) AS totaluser FROM users");
                                                    while ($fetch = $donor->fetch_array()) { ?>
                                                    <?php echo $fetch['totaluser'];?>
                                                    <?php } ?>
                                                </h2> </div>
														<div class="col-auto"> <i class="fas fa-gifts fa-2x text-gray-300"></i> </div>
													</div>
												</div>
											</div>
										</div>
                                    </div> <!-- END ROW  T25-->   
                                    
                                </div><!-- CLASS CONTAINER - FLUID -->
                                
								<?php include_once 'footer.php';?>
							</div><!-- END SECTION CONTENT-->
						</div><!-- END MAIN CONTENT-->
				</div><!-- END PAGE CONTAINER-->
		</div><!-- END PAGE WRAPPER-->
		<?php include_once 'jslink.php';?>
	</body>

	</html>
    <!-- end document-->
