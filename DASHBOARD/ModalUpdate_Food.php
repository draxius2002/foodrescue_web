<!-- REGISTER FORM FOOD MODAL -->
<?php

$i=1;
$sql = $connection->query("SELECT * FROM food LEFT JOIN category ON food.categoryID= category.categoryID");
while ($row = $sql->fetch_array()){
?>
                                            
<div class="modal fade" id="ModalUpdate_Food<?php echo $row['foodID']; ?>" tabindex="-1" role="dialog"
    aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-name" id="updateformLabel"> View Food </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!---Formbuilder Form--->
                <form id="insert_form" action="" method="" enctype="multipart/form-data"
                    class="mbr-form form-with-styler">
                   
                    <div class="row">
                        <div hidden="hidden" data-form-alert="" class="alert alert-success col-12">
                            Post success to save</div>
                        <div hidden="hidden" data-form-alert-danger="" class="alert alert-danger col-12">
                        </div>
                    </div>
                    <div class="dragArea row">

                        <div class="col-md-12  form-group" style="text-align:center">
                            <img src="images/<?php echo $row['image']; ?>"
                                style="border-radius: 25px; padding: 10px; width: 350px; height: 250px;"> </td>

                        </div>
                                    
                        <div class="col-md-6  form-group">
                            <label for="name-form1-h" class="form-control-label ">Name</label>
                            <input type="text" name="name" readonly value="<?php echo $row['name']; ?>"
                                class="form-control " id="name">
                        </div>
                        
                        <div class="col-md-3  form-group">
                            <label for="datereg-form1-h" class="form-control-label ">Quantity</label>
                            <input type="text" name="datepost" class="form-control" id="quantity" value="<?php echo $row['quantity']; ?>" readonly>
                        </div>
                        <div class="col-md-3  form-group">
                            <label for="datereg-form1-h" class="form-control-label ">Type</label>
                         
                            <input type="text" name="datepost" class="form-control" id="cat" value="<?php echo $row['title']; ?>" readonly>
                        </div>

                        <div class="col-md-6  form-group">
                            <label for="datereg-form1-h" class="form-control-label ">Start Available Date</label>
                            <input type="text" name="datepost" class="form-control" id="startavailable" value="<?php echo $row['startavailable']; ?>" readonly>
                        </div>

                        <div class="col-md-6  form-group">
                            <label for="datereg-form1-h" class="form-control-label ">End Available Date</label>
                            <input type="text" name="datepost" class="form-control" id="endavailable" value="<?php echo $row['endavailable']; ?>" readonly>
                        </div>


                        <div class="col-md-12  form-group">
                            <label for="name-form1-h" class="form-control-label ">Description</label>
                            <input  type="text" class="form-control" name="description"  id="description" readonly
                            value="<?php echo $row['description']; ?>">
                        </div>

                        <input type="hidden" name="foodID" id="foodID" />
                        <div class="modal-footer">
                            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
                            <button type="submit" value="Insert" id="insert" class="btn btn-primary"
                                name="submit">OK</button>
                        </div>
                    </div>
                </form>
                <!---Formbuilder Form--->
            </div>

        </div>
    </div>
</div> <!-- END REGISTER FORM FOOD MODAL -->
<?php
$i++;
}
?>