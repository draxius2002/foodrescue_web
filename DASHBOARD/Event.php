<?php

  include('Xconnection.php');
  session_start();
//   include('Vstyle.php');
?>
	<!DOCTYPE html>
	<html lang="en">
	<!-- Title Page-->
	<title>Event</title>
	<head>


<link href='fullcalendar/packages/core/main.css' rel='stylesheet' />
<link href='fullcalendar/packages/daygrid/main.css' rel='stylesheet' />
<link href='fullcalendar/packages/list/main.css' rel='stylesheet' />

<script src='fullcalendar/packages/core/main.js'></script>
<script src='fullcalendar/packages/list/main.js'></script>
<script src='fullcalendar/packages/daygrid/main.js'></script>
<script src='fullcalendar/packages/google-calendar/main.js'></script>
<script>
	document.addEventListener('DOMContentLoaded', function() {
		var calendarEl = document.getElementById('calendar');

		var calendar = new FullCalendar.Calendar(calendarEl, {
			plugins: ['dayGrid', 'googleCalendar', 'list'],
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
			},
			googleCalendarApiKey: 'AIzaSyC5Pt2AlrJuX_-RGn1QRJLnZQhY2knkywg',
			events: {
				googleCalendarId: '28kb1fnfa7dou3ceuq36e0m6js@group.calendar.google.com'
			}
		});

		calendar.render();
	});

	calendar.setOption('contentHeight', 200);
</script>
</head>

	<body>
		<div class="page-wrapper">
			<!-- HEADER MOBILE-->
			<!-- END HEADER MOBILE-->
			<?php include_once'header.php';?>
				<!-- MENU SIDEBAR-->
				<!-- END MENU SIDEBAR-->
				<!-- PAGE CONTAINER-->
				<div class="page-container">
					<!-- PAGE CONTAINER-->
					<!-- HEADER DESKTOP-->
					<?php include_once'headerD.php'; ?>
						<!-- END HEADER DESKTOP-->
						<!-- MAIN CONTENT-->
						<div class="main-content">
							<div class="section__content section__content--p30">
								<div class="container-fluid">
									<div class="row mb-3">
										<div class="col-md-12">
											<div class="overview-wrap">
												<h2 class="title-1">Food Rescue - Event</h2> </div>
										</div>
									</div>
									<!-- DataTales Example background-color:#e0334a-->
									<div class="card shadow mb-4" style="border-radius:10px">
										<div class="card-body">
										<div class="col-12">
                <div id='calendar'></div>
				</div>
										</div>
									</div>
								</div>
								<?php include_once'footer.php';
								?>
							</div>
							<!-- END SECTION CONTENT-->
						</div>
						<!-- END MAIN CONTENT-->
				</div>
				<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE WRAPPER-->
	</body>

	</html>
	<!-- end document-->