<?php
           include('Xconnection.php');
           session_start();
       // Now we check if the data from the login form was submitted, isset() will check if the data exists.
       if ( !isset($_POST['email'], $_POST['password']) ) {
	// Could not get the data that should have been sent.
       echo "<script type='text/javascript'>alert('Please fill both the username and password field!');
       window.location='../index.php';
       </script>";
       }

       // Prepare our SQL, preparing the SQL statement will prevent SQL injection.
       if ($stmt = $connection->prepare('SELECT id, password FROM admin WHERE email = ?')) {
	// Bind parameters (s = string, i = int, b = blob, etc), in our case the username is a string so we use "s"
	$stmt->bind_param('s', $_POST['email']);
	$stmt->execute();
	// Store the result so we can check if the account exists in the database.
       $stmt->store_result();
       
       if ($stmt->num_rows > 0) {
              $stmt->bind_result($id, $password);
              $stmt->fetch();
              // Account exists, now we verify the password.
              // Note: remember to use password_hash in your registration file to store the hashed passwords.
              if ($_POST['password'] === $password) {
                     // Verification success! User has loggedin!
                     // Create sessions so we know the user is logged in, they basically act like cookies but remember the data on the server.
                     session_regenerate_id();
                     $_SESSION['loggedin'] = TRUE;
                     $_SESSION['email'] = $_POST['email'];
                     $_SESSION['id'] = $id;
                     header('Location: index.php');
              } else {
                     echo 'Incorrect password!';
              }
       } else {
              echo 'Incorrect username!';
       }
       $stmt->close();

       }


           
?>
