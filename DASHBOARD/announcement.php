<?php

include('Xconnection.php');
session_start();
include('Vstyle.php');
?>
<!DOCTYPE html>
<html lang="en">
<!-- Title Page-->
<title>Announcement</title>

<body>
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->
		<!-- END HEADER MOBILE-->
		<?php include_once 'header.php'; ?>
		<!-- MENU SIDEBAR-->
		<!-- END MENU SIDEBAR-->
		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- PAGE CONTAINER-->
			<!-- HEADER DESKTOP-->
			<?php include_once 'headerD.php'; ?>
			<!-- END HEADER DESKTOP-->
			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="row mb-3">
							<div class="col-md-12">
								<div class="overview-wrap">
									<h2 class="title-1">Announcement</h2>
								</div>
							</div>
						</div>
						<!-- DataTales Example background-color:#e0334a-->
						<div class="card shadow mb-4" style="border-radius:10px">
							<div class="card-body">
								<div class="table-responsive" id="announcement">
									<ul class="list-group">
										<li class="list-group-item">3 new foods coming in today</li>
										<li class="list-group-item list-group-item-primary">20 pack of nasi lemak available</li>
										<li class="list-group-item list-group-item-secondary">24 sachet of milo 3in1 available for booking</li>
										<li class="list-group-item list-group-item-success">A jar of strawberry peanut butter available</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<?php include_once 'footer.php'; ?>
				</div>
				<!-- END SECTION CONTENT-->
			</div>
			<!-- END MAIN CONTENT-->
		</div>
		<!-- END PAGE CONTAINER-->
	</div>
	<!-- END PAGE WRAPPER-->
	<?php include_once 'jslink.php'; ?>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js">
	</script>
</body>

</html>
<!-- end document-->

<!-- Script for header table No sort-->
<script type="text/javascript">
	$(document).ready(function() {
		$('#DataTable').DataTable();
	});
	$('#DataTable').dataTable({
		"order": [],
		"columnDefs": [{
			"targets": 'no-sort',
			"orderable": false,
		}]
	});
</script>
<!-- END Script for header table No sort-->
<!-- javascript For update data through modal-->
<?php include_once 'ModalUpdate_Food.php'; ?>
<!-- End javascript For update data through modal-->
<!-- javascript For update data through modal-->
<?php include_once 'ModalDelete_Food.php'; ?>
<!-- End javascript For update data through modal-->
<!--javascript For uploading image-->
<script>
	$(document).ready(function() {
		$('#insert').click(function() {
			var image_name = $('#image').val();
			if (image_name == '') {
				alert("Please Select Image");
				return false;
			} else {
				var extension = $('#image').val().split('.').pop().toLowerCase();
				if (jQuery.in_array(extension, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
					alert('Invalid Image File');
					$('#image').val('');
					return false;
				}
			}
		});
	});
</script>
<!--End of javascript For uploading image-->


<?php
// sql to delete a record
if (mysqli_query($connection, $sql)) {
	echo "Record deleted successfully";
} else {
	echo "Error deleting record: " . mysqli_error($connection);
}
mysqli_close($connection);
?>



<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="js/sweetalert.min.js"></script>
<?php
if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
?>
	<script type='text/javascript'>
		swal({
			title: "<?php echo $_SESSION['status']; ?>",
			icon: "<?php echo $_SESSION['status_code']; ?>",
		});
	</script>
<?php
	unset($_SESSION['status']);
}
?>