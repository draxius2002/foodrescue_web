<!-- REGISTER FORM HOMELESS MODAL -->
<?php
$sql = 'SELECT * FROM users';
$result = mysqli_query($connection, $sql);
$i = 1;
while ($row = mysqli_fetch_array($result)) {
?>

<div class="modal fade" id="ModalUpdate_User<?php echo $row['userID']; ?>" tabindex="-1" role="dialog"
    aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-name" id="updateformLabel"> View User </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!---Formbuilder Form--->
                <form id="insert_form" action="Xupdatepost.php" method="POST" enctype="multipart/form-data"
                    class="mbr-form form-with-styler">
                   
                    <div class="row">
                        <div hidden="hidden" data-form-alert="" class="alert alert-success col-12">
                            Post success to save</div>
                        <div hidden="hidden" data-form-alert-danger="" class="alert alert-danger col-12">
                        </div>
                    </div>
                    <div class="dragArea row">

                        <div class="col-md-12  form-group" style="text-align:center">
                            <img src="../img/imgUser/<?php echo $row['image']; ?>"
                                style="border-radius: 25px; padding: 10px; width: 350px; height: 250px;"> </td>

                        </div>
                                    
                        <div class="col-md-6  form-group">
                            <label for="name-form1-h" class="form-control-label ">Name</label>
                            <input type="text" name="name" readonly value="<?php echo $row['name']; ?>"
                                class="form-control " id="name">
                        </div>
                        <div class="col-md-6  form-group">
                            <label for="name-form1-h" class="form-control-label ">Phone Number</label>
                            <input  type="text" class="form-control" name="phone"  id="phone" readonly
                            value="<?php echo $row['phoneNo']; ?>">
                        </div>

                        <div class="col-md-12  form-group">
                            <label for="datereg-form1-h" class="form-control-label ">Address</label>
                            <input type="text" name="datepost" class="form-control" value="<?php echo $row['address']; ?>" readonly>
                        </div>

                        <input type="hidden" name="userID" id="userID" />
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" value="Insert" id="insert" class="btn btn-primary"
                                name="submit">Save</button>
                        </div>
                    </div>
                </form>
                <!---Formbuilder Form--->
            </div>

        </div>
    </div>
</div> <!-- END REGISTER FORM HOMELESS MODAL -->
<?php
$i++;
}
?>